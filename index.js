const str = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut suscipit condimentum justo eget volutpat. Morbi felis diam, pellentesque sed aliquam eu, aliquam vitae elit. Integer ut sapien est. Nulla at elit nec nisl dignissim viverra sit amet in libero. Nunc sed bibendum enim. Praesent pharetra semper nulla vitae interdum. Nunc interdum enim imperdiet porta bibendum. Aenean ex ipsum, placerat sed sollicitudin et, tempus non sapien. Suspendisse congue vulputate molestie. Cras auctor vehicula justo tincidunt accumsan. Pellentesque vel iaculis nibh. Aliquam scelerisque eleifend vestibulum.

Phasellus non dictum eros.Praesent cursus laoreet ipsum, in porta nisi hendrerit eu.Pellentesque scelerisque felis ut nunc sagittis, quis ultricies nunc euismod.Curabitur quis neque in magna efficitur luctus mollis vel odio.In eu condimentum orci.Curabitur ut ex imperdiet, consectetur diam at, vestibulum risus.Nunc pharetra, est eu placerat dapibus, risus odio blandit ex, at aliquam enim augue sit amet lacus.Cras bibendum, quam non ultrices porttitor, leo urna egestas eros, a sagittis ligula erat vitae purus.In sit amet porta turpis.

Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae Cras mauris mi, aliquet ac dui non, pellentesque venenatis metus.Integer hendrerit tortor id pharetra ultrices.Suspendisse cursus suscipit congue.Vestibulum ornare faucibus interdum.Aliquam dapibus elit sed lorem laoreet tincidunt.Duis et sem fermentum urna tincidunt rutrum sit amet volutpat elit.`.toLocaleLowerCase();

let novaString = '';

for (char of str) {
    switch(char) {
        case 'a': novaString += '1'; break;
        case 'b': novaString += '2'; break;
        case 'c': novaString += '3'; break;
        case 'd': novaString += '4'; break;
        case 'e': novaString += '5'; break;
        case 'f': novaString += '6'; break;
        case 'g': novaString += '7'; break;
        case 'h': novaString += '8'; break;
        case 'i': novaString += '9'; break;
        case 'j': novaString += '10'; break;
        case 'k': novaString += '11'; break;
        case 'l': novaString += '12'; break;
        case 'm': novaString += '13'; break;
        case 'n': novaString += '14'; break;
        case 'o': novaString += '15'; break;
        case 'p': novaString += '16'; break;
        case 'q': novaString += '17'; break;
        case 'r': novaString += '18'; break;
        case 's': novaString += '19'; break;
        case 't': novaString += '20'; break;
        case 'u': novaString += '21'; break;
        case 'v': novaString += '22'; break;
        case 'w': novaString += '23'; break;
        case 'x': novaString += '24'; break;
        case 'y': novaString += '25'; break;
        case 'z': novaString += '26'; break;
        case '.': novaString += '0'; break;
        case ',': novaString += '-1'; break;
    }
}

console.log(novaString)